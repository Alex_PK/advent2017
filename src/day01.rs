pub fn day01(input: &str) -> u64 {
    format!("{}{}", input, input.chars().nth(0).unwrap())
        .split("")
        .filter(|v| !v.is_empty())
        .map(|v| v.parse::<u64>().unwrap())
        .collect::<Vec<_>>()
        .windows(2)
        .map(|v| if v[0] == v[1] { v[0] } else { 0 })
        .fold(0, |prev, v| v + prev)
}

pub fn day01_2(input: &str) -> u64 {
    let (a, b) = input.split_at(input.len() / 2);
    format!("{}{}", a, b).split("")
        .zip(format!("{}{}", b, a).split(""))
        .filter(|&(a, b)| !(a.is_empty() || b.is_empty()))
        .map(|(a, b)| (a.parse::<u64>().unwrap(), b.parse::<u64>().unwrap()))
        .map(|(a, b)| if a == b { a } else { 0 })
        .fold(0, |prev, v| v + prev)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_day01_1() {
        assert_eq!(3, day01("1122"));
        assert_eq!(4, day01("1111"));
        assert_eq!(0, day01("1234"));
        assert_eq!(9, day01("91212129"));
    }

    #[test]
    fn test_day01_2() {
        assert_eq!(6, day01_2("1212"));
        assert_eq!(0, day01_2("1221"));
        assert_eq!(4, day01_2("123425"));
        assert_eq!(12, day01_2("123123"));
        assert_eq!(4, day01_2("12131415"));
    }
}
