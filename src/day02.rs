pub fn day02_1(input: &str) -> u64 {
    input
        .split("\n")
        .filter(|v| !v.is_empty())
        .map(|v| v.split_whitespace().filter_map(|v| v.parse::<u64>().ok()).collect::<Vec<u64>>())
        .map(|v| (v.iter().max().unwrap() - v.iter().min().unwrap()))
        .fold(0, |acc, v| acc + v)
}

pub fn day02_2(input: &str) -> u64 {
    input
        .split("\n")
        .filter(|v| !v.is_empty())
        .map(|v| v.split_whitespace().filter_map(|v| v.parse::<u64>().ok()).collect::<Vec<u64>>())
        .map(|row| {
            for i in 0..row.len() {
                for j in 0..row.len() {
                    if i == j {
                        continue;
                    }
                    if row[i] % row[j] == 0 {
                        return row[i] / row[j];
                    }
                }
            }
            return 0;
        })
        .fold(0, |acc, v| acc + v)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_day02_1() {
        assert_eq!(18, day02_1(
            r#"5 1 9 5
7 5 3
2 4 6 8"#))
    }

    #[test]
    fn test_day02_2() {
        assert_eq!(9, day02_2(
            r#"5 9 2 8
9 4 7 3
3 8 6 5"#
        ))
    }
}
